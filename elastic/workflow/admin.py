# -*- coding: utf-8 -*-

from django.contrib import admin
from forms import *
from django.utils.translation import ugettext_lazy as _

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Elastic workflow package"


class TransitionAdminInline(admin.StackedInline):
    model = Transition
    extra = 1
    fk_name = 'from_state'
    form = TransitionInlineForm
    sortable_field_name = 'weight'
    fields = ['to_state', 'weight', 'condition_script', 'task_script', ]


class StateAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'workflow_schema', )
    list_display_links = ('name', )
    form = StateForm
    search_fields = ('name', 'workflow_schema__sku', )
    list_filter = ('workflow_schema', )

    inlines = [
        TransitionAdminInline,
    ]

admin.site.register(State, StateAdmin)


class SchemaAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', )
    list_display_links = ('name', )

admin.site.register(Schema, SchemaAdmin)


class WorkflowLogRecordAdminInline(admin.TabularInline):
    model = WorkflowLogRecord
    readonly_fields = ('creation_date', 'state_name', 'state_code', )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


def workflow_start(modeladmin, request, queryset):
    for workflow in queryset.iterator():
        workflow.start()
workflow_start.short_description = _('Workflow processing')


class WorkflowAdmin(admin.ModelAdmin):
    list_display = ('id', 'workflow_schema', 'get_current_state_name', 'original_document', )
    list_display_links = ('id', )
    readonly_fields = ('workflow_schema', 'original_document', )
    exclude = ('doc_content_type', 'doc_id', )
    list_filter = ('workflow_schema', )

    form = WorkflowForm

    def has_add_permission(self, request, obj=None):
        return False

    inlines = [
        WorkflowLogRecordAdminInline,
    ]

    actions = [workflow_start, ]

admin.site.register(Workflow, WorkflowAdmin)
