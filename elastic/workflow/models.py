# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from exceptions import *
from django.core.exceptions import MultipleObjectsReturned
from django.utils import translation
from django.conf import settings
from django.db import transaction

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Elastic workflow package"


class Schema(models.Model):
    """
    Схема бизнес-процесса
    """
    class Meta:
        verbose_name = _('Workflow schema')
        verbose_name_plural = _('Workflow schemas')

    #: Наименование
    name = models.CharField(max_length=255, verbose_name=_('Name'))

    #: Код
    code = models.CharField(verbose_name=_('Code'), unique=True, max_length=128, db_index=True)

    def __unicode__(self):
        return u"{0}".format(self.name)

    def get_initial_state(self):
        try:
            return self.state_set.get(init=True)
        except MultipleObjectsReturned:
            raise WorkflowConfigurationError("Initial state not more than one!")


class State(models.Model):
    """
    Уровень (состояние) в схеме бизнес-процесса
    """
    class Meta:
        verbose_name = _('Workflow state')
        verbose_name_plural = _('Workflow states')
        unique_together = (('code', 'workflow_schema'), )

    def limit_workflow_schema_choices(self):
        return {'workflow_schema__id': self.workflow_schema.id}

    #: Наименование
    name = models.CharField(max_length=255, verbose_name=_('Name'))

    #: Код
    code = models.CharField(verbose_name=_('Code'), max_length=128, db_index=True)

    init = models.BooleanField(default=False, verbose_name=_('Init state'))

    #: Скрипт-задача, исполняемая при прерывании бизнес-процесса
    brake_task_script = models.TextField(verbose_name=_('Brake task'))

    descendants = models.ManyToManyField('self', blank=True, through="Transition", symmetrical=False,
                                         limit_choices_to=limit_workflow_schema_choices,
                                         verbose_name=_('Descendants'))

    #: Схема бизнес-процесса
    workflow_schema = models.ForeignKey(Schema, verbose_name=_('Workflow schema'))

    def __unicode__(self):
        return u"{0} - {1}".format(self.name, self.workflow_schema)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Хотя бы одно состояние должно быть помечено как стартовое
        if State.objects.filter(workflow_schema=self.workflow_schema, init=True).count() == 0:
            self.init = True

        super(State, self).save(force_insert, force_update, using, update_fields)

        # Если текущее состояние объявлено как стартовое, то у всех иных состояний даного БП
        # аналогичный флаг должен быть сброшен
        if self.init:
            for state in State.objects.filter(workflow_schema=self.workflow_schema):
                if state != self:
                    state.init = False
                    state.save()


class Transition(models.Model):
    """
    Переход между состояниями в схеме бизнес-процесса
    """
    class Meta:
        verbose_name = _('Transition')
        verbose_name_plural = _('Transitions')

    #: Со состояния
    from_state = models.ForeignKey(State, related_name='from_transitions', verbose_name=_('From state'))

    #: На состояние
    to_state = models.ForeignKey(State, related_name='to_transitions', verbose_name=_('To state'))

    #: Вес (приоритет исполнения)
    weight = models.IntegerField(default=10, verbose_name=_('Weight'))

    #: Скрипт-проверка условий перехода между состояниями в схеме бизнес-процесса
    condition_script = models.TextField(verbose_name=_('Condition'))

    #: Скрипт-задача, исполняемая при переходе на новый уровень в схеме бизнес-процесса
    task_script = models.TextField(verbose_name=_('Task'))

    def __unicode__(self):
        return u"{0} - {1}".format(self.from_state.name, self.to_state.name)


class Workflow(models.Model):
    """
    Экземпляр бизнес-процесса
    """
    class Meta:
        verbose_name = _('Workflow instance')
        verbose_name_plural = _('Workflow instances')

    #: Тип экземпляра ассоциированного документа
    doc_content_type = models.ForeignKey(ContentType, null=True, blank=True)

    #: Идентификатор ассоциированного экземпляра документа
    doc_id = models.PositiveIntegerField(verbose_name=_('Document ID'), null=True, blank=True)

    #: Экземпляр ассоциированного документа
    doc_object = GenericForeignKey('doc_content_type', 'doc_id')

    #: Схема бизнес-процесса
    workflow_schema = models.ForeignKey(Schema, verbose_name=_('Workflow schema'))

    def original_document(self):
        if not self.doc_object:
            return None
        language = getattr(settings, 'LANGUAGE_CODE', 'en')
        with translation.override(language):
            return u"{0} {1}".format(
                translation.ugettext(self.doc_object._meta.verbose_name.capitalize()), self.doc_object)
    original_document.short_description = _('Original document')

    def doc_code(self):
        return self.doc_object.code
    doc_code = property(doc_code)

    def log_creation_date(self):
        return self.log.order_by('creation_date').last().creation_date
    log_creation_date = property(log_creation_date)

    @staticmethod
    def run_script(script, from_state, to_state, workflow_instance):
        g = {}
        exec compile(script, '<string>', 'exec') in g

        if 'handler' not in g or not callable(g['handler']):
            raise ScriptRuntimeError("Script must have name 'handler'!")

        try:
            return g['handler'](from_state, to_state, workflow_instance)
        except Exception as e:
            print str(e)
            raise ScriptRuntimeError(e.message)

    @transaction.atomic
    def start(self):
        """
        Начать/продолжить бизнес-процесс
        """
        log_record = self.log.order_by("creation_date").last()
        current_state = State.objects.get(code=log_record.state_code, workflow_schema=self.workflow_schema)
        for transition in current_state.from_transitions.order_by('weight'):
            if transition.condition_script:
                # Если выполняется условие ребра
                print "выполняется условие ребра {0}".format(transition)
                if self.run_script(transition.condition_script, transition.from_state, transition.to_state, self):
                    print "Выполняем задачу ребра {0}".format(transition)
                    # Выполняем задачу ребра
                    self.run_script(transition.task_script, transition.from_state, transition.to_state, self)
                    WorkflowLogRecord.objects.create(
                        state_name=transition.to_state.name, state_code=transition.to_state.code, workflow=self)

                    # Запускаем бизнес-процесс рекурсивно
                    self.start()
                    break

    def get_current_state_name(self):
        return self.log.order_by('creation_date').last().state_name
    get_current_state_name.short_description = _('Current state name')

    def get_current_state_code(self):
        return self.log.order_by('creation_date').last().state_code
    get_current_state_name.short_description = _('Current state code')

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Workflow, self).save(force_insert, force_update, using, update_fields)
        if self.log.count() == 0:
            init_state = self.workflow_schema.get_initial_state()
            WorkflowLogRecord.objects.create(state_name=init_state.name, state_code=init_state.code, workflow=self)

    def delete(self, using=None):
        log_record = self.log.order_by("creation_date").last()
        current_state = State.objects.get(code=log_record.state_code, workflow_schema=self.workflow_schema)
        g = {}
        exec compile(current_state.brake_task_script, '<string>', 'exec') in g

        if 'handler' not in g or not callable(g['handler']):
            raise ScriptRuntimeError("Script must have name 'handler'!")

        try:
            if g['handler'](self):
                super(Workflow, self).delete(using)
        except Exception as e:
            print str(e)
            raise ScriptRuntimeError(e.message)


class WorkflowLogRecord(models.Model):
    """
    Запись журнала экземпляра бизнес-процесса
    """

    #: Дата создания
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Creation date'))

    #: Наименование состояния бизнес-процесса
    state_name = models.CharField(max_length=255, verbose_name=_('State name'))

    #: Код состояния бизнес-процесса
    state_code = models.CharField(max_length=128, verbose_name=_('State code'))

    #: Экземпляр бизнес-процесс
    workflow = models.ForeignKey(Workflow, related_name='log', verbose_name=_('Workflow'))
