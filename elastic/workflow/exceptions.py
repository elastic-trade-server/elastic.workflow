# -*- coding: utf-8 -*-

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Elastic workflow package"


class WorkflowConfigurationError(Exception):
    def __init__(self, msg):
        self.msg = msg
        super(WorkflowConfigurationError, self).__init__()

    def __unicode__(self):
        return unicode(self.msg)


class ScriptRuntimeError(Exception):
    def __init__(self, msg):
        self.msg = msg
        super(ScriptRuntimeError, self).__init__()

    def __unicode__(self):
        return unicode(self.msg)
