# -*- coding: utf-8 -*-


from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Elastic workflow package"


class ElasticWorkflow(AppConfig):
    name = 'elastic.workflow'
    label = "workflow"
    verbose_name = _('Workflow')
