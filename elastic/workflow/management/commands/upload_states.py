# -*- coding: utf-8 -*-

import os
from django.core.management.base import BaseCommand, CommandError
from django.core import serializers
from django.db import connection
from django.db.transaction import atomic
from elastic.workflow.models import State


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('file')

    @atomic()
    def handle(self, *args, **options):
        if not os.path.exists(options['file']):
            raise CommandError('File not exists: %s' % options['file'])

        State.objects.all().delete()
        table_names = set()

        with connection.constraint_checks_disabled():
            format = self.parse_format(os.path.basename(options['file']))

            for obj in serializers.deserialize(format, open(options['file'], 'r')):
                table_names.add(obj.object.__class__._meta.db_table)
                obj.save()

        connection.check_constraints(table_names=table_names)

    def parse_format(self, fixture_name):
        parts = fixture_name.rsplit('.', 2)
        fixture_format = None
        if len(parts) > 1:
            if parts[-1] in serializers.get_public_serializer_formats():
                fixture_format = parts[-1]
            else:
                raise CommandError("Invalid serializer format %s" % parts[-1])
        return fixture_format
