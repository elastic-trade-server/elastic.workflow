# -*- coding: utf-8 -*-

from django.forms import CharField
from models import *
import django
from autocomplete_light import forms
from django_ace import AceWidget


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Elastic workflow package"


brake_task_script_template_code =\
    """############# Задача исполняемая при прерывании
## Эта функция содержит произвольный код-задачу исполняемую при прерывании.
## Функция должна вернуть Истину, в случае удачного завершения, или Ложь -  в ином случае
## Функция получает в качестве параметра ссылку на эеземпляр БП
def handler(workflow_instance):
    return True
"""


class StateForm(forms.ModelForm):
    class Meta:
        model = State
        if django.VERSION >= (1, 6):
            fields = '__all__'
        # autocomplete_exclude = ['description', ]

    brake_task_script = CharField(
        label=u"Your content",
        widget=AceWidget(mode='python', theme='chrome', width="800px"),
        initial=brake_task_script_template_code,
        show_hidden_initial=True
    )


condition_script_template_code =\
    """############# Условие ребра
## Эта функция должна вернуть Истину если переход по ребру допустим, или Ложь - в ином случае
## Функция получает в качестве параметров состояние-источник, состояние-получатель и ссылку на эеземпляр БП
def handler(from_state, to_state, workflow_instance):
    return True
"""

task_script_template_code =\
    """############# Задача исполняемая при переходе
## Эта функция содержит произвольный код-задачу исполняемую при переходе.
## Функция должна вернуть Истину, в случае удачного завершения, или Ложь -  в ином случае
## Функция получает в качестве параметров состояние-источник, состояние-получатель и ссылку на эеземпляр БП
def handler(from_state, to_state, workflow_instance):
    return True
"""


class TransitionInlineForm(forms.ModelForm):
    class Meta:
        model = Transition
        if django.VERSION >= (1, 6):
            fields = '__all__'
        autocomplete_exclude = ['from_state', ]

    condition_script = CharField(
        label=u"Your content",
        widget=AceWidget(mode='python', theme='chrome', width="800px"),
        initial=condition_script_template_code,
        show_hidden_initial=True
    )

    task_script = CharField(
        label=u"Your content",
        widget=AceWidget(mode='python', theme='chrome', width="800px"),
        initial=task_script_template_code,
        show_hidden_initial=True
    )


class WorkflowForm(forms.ModelForm):
    class Meta:
        model = Workflow
        if django.VERSION >= (1, 6):
            fields = '__all__'
        autocomplete_exclude = ['doc_object', ]

