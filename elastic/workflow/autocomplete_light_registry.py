# -*- coding: utf-8 -*-

import autocomplete_light.shortcuts as autocomplete_light
from models import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Elastic workflow package"


autocomplete_light.register(
    Schema,
    search_fields=['name', ],
    attrs={
        'placeholder': 'Схема бизнес-процесса....',
        'data-autocomplete-minimum-characters': 2,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)

autocomplete_light.register(
    State,
    search_fields=['name', ],
    attrs={
        'placeholder': 'Состояние бизнес-процесса....',
        'data-autocomplete-minimum-characters': 2,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)
