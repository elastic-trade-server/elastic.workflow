# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0002_auto_20160414_0027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workflow',
            name='solution',
            field=models.CharField(max_length=16, null=True, verbose_name='Solution', blank=True),
        ),
    ]
