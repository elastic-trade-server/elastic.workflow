# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0007_auto_20160414_0658'),
    ]

    operations = [
        migrations.CreateModel(
            name='WorkflowLogRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('state_name', models.CharField(max_length=255, verbose_name='State name')),
                ('state_code', models.CharField(max_length=128, verbose_name='State code')),
            ],
        ),
        migrations.RemoveField(
            model_name='workflow',
            name='solution',
        ),
        migrations.RemoveField(
            model_name='workflow',
            name='state_code',
        ),
        migrations.RemoveField(
            model_name='workflow',
            name='state_name',
        ),
        migrations.AddField(
            model_name='workflowlogrecord',
            name='workflow',
            field=models.ForeignKey(verbose_name='Workflow', to='workflow.Workflow'),
        ),
    ]
