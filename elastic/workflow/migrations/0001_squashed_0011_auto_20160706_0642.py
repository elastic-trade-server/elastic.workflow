# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    replaces = [
        (b'workflow', '0001_initial'),
        (b'workflow', '0002_auto_20160414_0027'),
        (b'workflow', '0003_auto_20160414_0030'),
        (b'workflow', '0004_auto_20160414_0201'),
        (b'workflow', '0005_auto_20160414_0216'),
        (b'workflow', '0006_auto_20160414_0641'),
        (b'workflow', '0007_auto_20160414_0658'),
        (b'workflow', '0008_auto_20160415_0432'),
        (b'workflow', '0009_auto_20160427_0421'),
        (b'workflow', '0010_auto_20160427_0455'),
        (b'workflow', '0011_auto_20160706_0642')
    ]

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schema',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('code', models.CharField(max_length=128, verbose_name='Code', db_index=True)),
            ],
            options={
                'verbose_name': 'Workflow',
                'verbose_name_plural': 'Workflow',
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('code', models.CharField(max_length=128, verbose_name='Code', db_index=True)),
            ],
            options={
                'verbose_name': 'Workflow state',
                'verbose_name_plural': 'Workflow states',
            },
        ),
        migrations.CreateModel(
            name='Transition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weight', models.IntegerField(default=10, verbose_name='Weight')),
                ('condition_script', models.TextField(verbose_name='Condition')),
                ('task_script', models.TextField(verbose_name='Task')),
                ('from_state', models.ForeignKey(related_name='from_transitions', verbose_name='From state', to='workflow.State')),
                ('to_state', models.ForeignKey(related_name='to_transitions', verbose_name='To state', to='workflow.State')),
            ],
            options={
                'verbose_name': 'Transition',
                'verbose_name_plural': 'Transitions',
            },
        ),
        migrations.CreateModel(
            name='Workflow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('workflow_schema', models.ForeignKey(verbose_name='Workflow schema', to='workflow.Schema')),
                ('doc_content_type', models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True)),
                ('doc_id', models.PositiveIntegerField(null=True, verbose_name='Document ID', blank=True)),
            ],
            options={
                'verbose_name': 'Workflow instance',
                'verbose_name_plural': 'Workflow instances',
            },
        ),
        migrations.AddField(
            model_name='state',
            name='descendants',
            field=models.ManyToManyField(
                to=b'workflow.State', verbose_name='Descendants', through='workflow.Transition', blank=True),
        ),
        migrations.AddField(
            model_name='state',
            name='workflow_schema',
            field=models.ForeignKey(verbose_name='Workflow schema', to='workflow.Schema'),
        ),
        migrations.AlterModelOptions(
            name='schema',
            options={'verbose_name': 'Workflow schema', 'verbose_name_plural': 'Workflow schemas'},
        ),
        migrations.AddField(
            model_name='state',
            name='init',
            field=models.BooleanField(default=False, verbose_name='Init state'),
        ),
        migrations.CreateModel(
            name='WorkflowLogRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('state_name', models.CharField(max_length=255, verbose_name='State name')),
                ('state_code', models.CharField(max_length=128, verbose_name='State code')),
                ('workflow', models.ForeignKey(related_name='log', verbose_name='Workflow', to='workflow.Workflow')),
            ],
        ),
        migrations.AddField(
            model_name='state',
            name='brake_task_script',
            field=models.TextField(default='############# \u0417\u0430\u0434\u0430\u0447\u0430 \u0438\u0441\u043f\u043e\u043b\u043d\u044f\u0435\u043c\u0430\u044f \u043f\u0440\u0438 \u043f\u0440\u0435\u0440\u044b\u0432\u0430\u043d\u0438\u0438\n## \u042d\u0442\u0430 \u0444\u0443\u043d\u043a\u0446\u0438\u044f \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u0442 \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u043b\u044c\u043d\u044b\u0439 \u043a\u043e\u0434-\u0437\u0430\u0434\u0430\u0447\u0443 \u0438\u0441\u043f\u043e\u043b\u043d\u044f\u0435\u043c\u0443\u044e \u043f\u0440\u0438 \u043f\u0440\u0435\u0440\u044b\u0432\u0430\u043d\u0438\u0438.\n## \u0424\u0443\u043d\u043a\u0446\u0438\u044f \u0434\u043e\u043b\u0436\u043d\u0430 \u0432\u0435\u0440\u043d\u0443\u0442\u044c \u0418\u0441\u0442\u0438\u043d\u0443, \u0432 \u0441\u043b\u0443\u0447\u0430\u0435 \u0443\u0434\u0430\u0447\u043d\u043e\u0433\u043e \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u044f, \u0438\u043b\u0438 \u041b\u043e\u0436\u044c -  \u0432 \u0438\u043d\u043e\u043c \u0441\u043b\u0443\u0447\u0430\u0435\n## \u0424\u0443\u043d\u043a\u0446\u0438\u044f \u043f\u043e\u043b\u0443\u0447\u0430\u0435\u0442 \u0432 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u0435 \u043f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u0430 \u0441\u0441\u044b\u043b\u043a\u0443 \u043d\u0430 \u044d\u0435\u0437\u0435\u043c\u043f\u043b\u044f\u0440 \u0411\u041f\ndef handler(workflow_instance):\n    return True\n', verbose_name='Brake task'),
        ),
        migrations.AlterUniqueTogether(
            name='state',
            unique_together=set([('code', 'workflow_schema')]),
        ),
    ]
