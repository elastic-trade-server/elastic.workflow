# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0003_auto_20160414_0030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transition',
            name='to_state',
            field=models.ForeignKey(related_name='to_transitions', verbose_name='To state', blank=True, to='workflow.State', null=True),
        ),
    ]
