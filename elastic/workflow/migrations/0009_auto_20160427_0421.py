# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0008_auto_20160415_0432'),
    ]

    operations = [
        migrations.AddField(
            model_name='state',
            name='brake_task_script',
            field=models.TextField(null=True, verbose_name='Brake task', blank=True),
        ),
        migrations.AlterField(
            model_name='workflowlogrecord',
            name='workflow',
            field=models.ForeignKey(related_name='log', verbose_name='Workflow', to='workflow.Workflow'),
        ),
    ]
