# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0009_auto_20160427_0421'),
    ]

    operations = [
        migrations.AlterField(
            model_name='state',
            name='brake_task_script',
            field=models.TextField(default="""############# Задача исполняемая при прерывании
## Эта функция содержит произвольный код-задачу исполняемую при прерывании.
## Функция должна вернуть Истину, в случае удачного завершения, или Ложь -  в ином случае
## Функция получает в качестве параметра ссылку на эеземпляр БП
def handler(workflow_instance):
    return True
""", verbose_name='Brake task'),
            preserve_default=False,
        ),
    ]
