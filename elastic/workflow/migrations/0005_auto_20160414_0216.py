# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0004_auto_20160414_0201'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transition',
            name='to_state',
            field=models.ForeignKey(related_name='to_transitions', verbose_name='To state', to='workflow.State'),
        ),
    ]
