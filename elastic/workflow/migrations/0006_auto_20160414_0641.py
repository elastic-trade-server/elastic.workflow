# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('workflow', '0005_auto_20160414_0216'),
    ]

    operations = [
        migrations.AddField(
            model_name='workflow',
            name='doc_content_type',
            field=models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='workflow',
            name='doc_id',
            field=models.PositiveIntegerField(null=True, verbose_name='Document ID', blank=True),
        ),
    ]
