# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0006_auto_20160414_0641'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='doc_content_type',
        ),
        migrations.RemoveField(
            model_name='document',
            name='workflow',
        ),
        migrations.DeleteModel(
            name='Document',
        ),
    ]
