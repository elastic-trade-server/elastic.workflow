# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc_id', models.PositiveIntegerField(verbose_name='Document ID')),
                ('doc_content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'Document',
                'verbose_name_plural': 'Documents',
            },
        ),
        migrations.CreateModel(
            name='Schema',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('code', models.CharField(max_length=128, verbose_name='Code', db_index=True)),
            ],
            options={
                'verbose_name': 'Workflow',
                'verbose_name_plural': 'Workflow',
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('code', models.CharField(max_length=128, verbose_name='Code', db_index=True)),
            ],
            options={
                'verbose_name': 'Workflow state',
                'verbose_name_plural': 'Workflow states',
            },
        ),
        migrations.CreateModel(
            name='Transition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weight', models.IntegerField(default=10, verbose_name='Weight')),
                ('condition_script', models.TextField(verbose_name='Condition')),
                ('task_script', models.TextField(verbose_name='Task')),
                ('from_state', models.ForeignKey(related_name='from_transitions', verbose_name='From state', to='workflow.State')),
                ('to_state', models.ForeignKey(related_name='to_transitions', verbose_name='To state', to='workflow.State')),
            ],
            options={
                'verbose_name': 'Transition',
                'verbose_name_plural': 'Transitions',
            },
        ),
        migrations.CreateModel(
            name='Workflow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state_name', models.CharField(max_length=255, verbose_name='State name')),
                ('state_code', models.CharField(max_length=128, verbose_name='State code')),
                ('solution', models.CharField(max_length=16, verbose_name='Solution')),
                ('workflow_schema', models.ForeignKey(verbose_name='Workflow schema', to='workflow.Schema')),
            ],
            options={
                'verbose_name': 'Workflow instance',
                'verbose_name_plural': 'Workflow instances',
            },
        ),
        migrations.AddField(
            model_name='state',
            name='descendants',
            field=models.ManyToManyField(to='workflow.State', verbose_name='Descendants', through='workflow.Transition', blank=True),
        ),
        migrations.AddField(
            model_name='state',
            name='workflow_schema',
            field=models.ForeignKey(verbose_name='Workflow schema', to='workflow.Schema'),
        ),
        migrations.AddField(
            model_name='document',
            name='workflow',
            field=models.ForeignKey(verbose_name='Workflow instance', to='workflow.Workflow'),
        ),
    ]
