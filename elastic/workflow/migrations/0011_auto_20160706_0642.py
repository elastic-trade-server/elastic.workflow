# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0010_auto_20160427_0455'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schema',
            name='code',
            field=models.CharField(max_length=128, verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='schema',
            name='code',
            field=models.CharField(unique=True, max_length=128, verbose_name='Code', db_index=True),
        ),
        migrations.AlterUniqueTogether(
            name='state',
            unique_together=set([('code', 'workflow_schema')]),
        ),
    ]
